# python-challenge

## Part 1

Write a CLI application in Python that stores accounting data in a YAML file (`data/account.yaml`).

A account entry in the end will look like this (for now):

```yaml
description: 'Something I bought'
gross: -99.99
invoice_date: 2020-11-24
invoice_id: some-invoice-id
net: -86.20
```

The file (`data/account.yaml`) comprises of a list of such entries:

```yaml
- description: 'Something I bought'
  gross: -99.99
  invoice_date: 2020-11-24
  invoice_id: some-invoice-no
  net: -86.20
- description: 'Some money a got'
  gross: 5000,00
  invoice_date: 2020-11-25
  invoice_id: some-other-invoice-no
  net: 4310,34
```

Income is represented by a positive entry, something you spent money for by a negative entry.

Implement a **first command** for your CLI that is able to add a single entry.
If no gross value is provided, a default should be used (19%).

Implement a **second command** that prints the overall balance of the whole account.
We define the balance of the account as sum of all entries.
Both net and gross value should be printed.

### Some tips
- Use [Pipenv](https://pipenv.pypa.io/en/latest/) for dependency management.
- [PyYAML](https://pypi.org/project/PyYAML/) is awesome for working with YAML in Python.
- [pytest](https://pypi.org/project/pytest/) is good for unit testing.
- An entry as mentioned above should be represented as a class.
- Stick with argparse for building the CLI – it's good enough

### Some more tips
Maybe add a file `accounting.sh` to the top that does something like this:
```shell script
#!/usr/bin/env bash

exec pipenv run python -m some.thing "$@"
```

The call for showing the account balance would then just look like this:
```shell script
./accounting.sh summary
```

The call for showing the account balance would then just look like this:
```shell script
./accounting.sh add --net -10.00 --gross -11.00 --description "foobar" #...
```

## Part 2
Pimp the summary to build a more useful output, e.g. something like this:

```shell script
====================================================================================
inv date   description                    invoice             net         gross
====================================================================================
23.11.2020 Foobar                         54321               -86.20€     -99.99€
24.11.2020 Barfoo                         12345               2000.00€    2000.00€
25.11.2020 XXXXXXX€                       AB666               -965.52€    -1120.00€
====================================================================================
            Balance at 25.11.2020                             XXXXXXX€   YYYYYYYYY€
------------------------------------------------------------------------------------
```
